package ru.kai.sunshine;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.net.URL;

import ru.kai.sunshine.data.SunshinePreferences;
import ru.kai.sunshine.data.WeatherContract;
import ru.kai.sunshine.sync.SunshineSyncUtils;
import ru.kai.sunshine.utilities.FakeDataUtils;
import ru.kai.sunshine.utilities.NetworkUtils;

public class MainActivity extends AppCompatActivity implements ForecastAdapter.ForecastAdapterOnClickHandler,
        LoaderManager.LoaderCallbacks<Cursor>{

    private static final String TAG = MainActivity.class.getSimpleName();

    public static final String[] MAIN_FORECAST_PROJECTION = {
            WeatherContract.WeatherEntry.COLUMN_DATE,
            WeatherContract.WeatherEntry.COLUMN_MAX_TEMP,
            WeatherContract.WeatherEntry.COLUMN_MIN_TEMP,
            WeatherContract.WeatherEntry.COLUMN_WEATHER_ID,
    };

    public static final int INDEX_WEATHER_DATE = 0;
    public static final int INDEX_WEATHER_MAX_TEMP = 1;
    public static final int INDEX_WEATHER_MIN_TEMP = 2;
    public static final int INDEX_WEATHER_CONDITION_ID = 3;


    private RecyclerView mRecyclerView;
    private int mPosition = RecyclerView.NO_POSITION;
    static ForecastAdapter mForecastAdapter;
    ProgressBar pb;

    Toast toast;
    static final int LOADERS_ID = 0;
    static final String BUNDLE_LOADER_URL = "bundleLoaderURL";
    LoaderManager lm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forecast);
        getSupportActionBar().setElevation(0f);

        pb = findViewById(R.id.pb);

        mRecyclerView = findViewById(R.id.recyclerview_forecast);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);

        mForecastAdapter = new ForecastAdapter(this,this);
        mRecyclerView.setAdapter(mForecastAdapter);

        showLoading();

        lm = getLoaderManager();

//        Bundle bundle = prepareBundleForLoader();
//        lm.initLoader(LOADERS_ID, bundle, this);
        lm.initLoader(LOADERS_ID, null, this);

        SunshineSyncUtils.initialize(this);
    }

    private void showLoading() {
        mRecyclerView.setVisibility(View.INVISIBLE);
        pb.setVisibility(View.VISIBLE);
    }
    private void showWeatherDataView(){
        pb.setVisibility(View.INVISIBLE);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    // click по item-у адаптера
    @Override
    public void onClickHandler(long date) {
        Intent weatherDetailIntent = new Intent(MainActivity.this, DetailActivity.class);
        Uri uriForDateClicked = WeatherContract.WeatherEntry.buildWeatherUriWithDate(date);
        weatherDetailIntent.setData(uriForDateClicked);
        startActivity(weatherDetailIntent);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int loaderId, final Bundle args) {

        switch (loaderId) {

            case LOADERS_ID:
                /* URI for all rows of weather data in our weather table */
                Uri forecastQueryUri = WeatherContract.WeatherEntry.WEATHER_CONTENT_URI;
                /* Sort order: Ascending by date */
                String sortOrder = WeatherContract.WeatherEntry.COLUMN_DATE + " ASC";
                /*
                 * A SELECTION in SQL declares which rows you'd like to return. In our case, we
                 * want all weather data from today onwards that is stored in our weather table.
                 * We created a handy method to do that in our WeatherEntry class.
                 */
                String selection = WeatherContract.WeatherEntry.getSqlSelectForTodayOnwards();

                return new CursorLoader(this,
                        forecastQueryUri,
                        MAIN_FORECAST_PROJECTION,
                        selection,
                        null,
                        sortOrder);

            default:
                throw new RuntimeException("Loader Not Implemented: " + loaderId);
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mForecastAdapter.swapCursor(data);

        if (mPosition == RecyclerView.NO_POSITION){
            mPosition = 0;
        }

        mRecyclerView.smoothScrollToPosition(mPosition);

        if (data.getCount() != 0){
            showWeatherDataView();
        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mForecastAdapter.swapCursor(null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.forecast, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item == null) {
            try {
                throw new NullPointerException("No menu item");
            } catch (NullPointerException e) {
                System.out.println(e.toString());
                e.printStackTrace();
                return false;
            }
        }
        switch (item.getItemId()){
            case R.id.action_open_map:
                openLocationInMap();
                return true;
            case R.id.action_open_settings:
                Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openLocationInMap() {
        //Uri uri = Uri.parse("geo:0,0?q=" + addressString);
        String defAddressString = "1600 Ampitheatre Parkway, CA";

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String addressString = sharedPreferences.getString(getString(R.string.pref_location_key), defAddressString);

        Uri.Builder builder = new Uri.Builder();
        builder.scheme("geo")
                .path("0,0")
//                .query(defAddressString)
                .appendQueryParameter("z","10")
                .appendQueryParameter("q", addressString); // тоже работает
        Uri uri = builder.build();
        Intent mapIntent = new Intent(Intent.ACTION_VIEW);
        mapIntent.setData(uri);

        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        } else {
            Log.d(TAG, "Couldn't call " + uri.toString() + ", no receiving apps installed!");
        }
    }

}
