package ru.kai.sunshine.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

/**
 * Created by dady on 13.01.2018.
 */

public class WeatherDbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "weather.db";
    private static final int DATABASE_VERSION = 3;

    private static final String CREATE_TABLE = "create table " + WeatherContract.WeatherEntry.TABLE_NAME + "("
            + BaseColumns._ID + " integer primary key autoincrement, "
            + WeatherContract.WeatherEntry.COLUMN_DATE + " integer not null, " // " timestamp default current_timestamp, "
            + WeatherContract.WeatherEntry.COLUMN_WEATHER_ID + " integer not null, "
            + WeatherContract.WeatherEntry.COLUMN_MIN_TEMP + " real not null, "
            + WeatherContract.WeatherEntry.COLUMN_MAX_TEMP + " real not null, "
            + WeatherContract.WeatherEntry.COLUMN_HUMIDITY + " real not null, "
            + WeatherContract.WeatherEntry.COLUMN_PRESSURE + " real not null, "
            + WeatherContract.WeatherEntry.COLUMN_WIND_SPEED + " real not null, "
            + WeatherContract.WeatherEntry.COLUMN_DEGREES + " real not null, "
            + " UNIQUE (" + WeatherContract.WeatherEntry.COLUMN_DATE + ") ON CONFLICT REPLACE);";


    public WeatherDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        final String DROP_TABLE = "drop table if exists "
                + WeatherContract.WeatherEntry.TABLE_NAME + ";";
        db.execSQL(DROP_TABLE);
        onCreate(db);
    }
}
