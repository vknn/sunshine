package ru.kai.sunshine.data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import ru.kai.sunshine.utilities.SunshineDateUtils;

/**
 * Created by dady on 21.01.2018.
 */

public class WeatherProvider extends ContentProvider {

    WeatherDbHelper mOpenHelper;

    public static final int CODE_WEATHER = 100;
    public static final int CODE_WEATHER_WITH_DATE = 101;

    private static final UriMatcher mUriMatcher = buildUriMatcher();

    public static UriMatcher buildUriMatcher(){
        UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(WeatherContract.AUTHORITY, WeatherContract.PATH, CODE_WEATHER);
        uriMatcher.addURI(WeatherContract.AUTHORITY, WeatherContract.PATH + "/#", CODE_WEATHER_WITH_DATE);

        return uriMatcher;
    }

    @Override
    public boolean onCreate() {

        Context context = getContext();
        mOpenHelper = new WeatherDbHelper(context);
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {

        SQLiteDatabase db = mOpenHelper.getReadableDatabase();
        int match = mUriMatcher.match(uri);

        Cursor cursor;
        switch (match) {
            case CODE_WEATHER:
                cursor = db.query(WeatherContract.WeatherEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            case CODE_WEATHER_WITH_DATE:
                String mSelection = WeatherContract.WeatherEntry.COLUMN_DATE + "=?";
                String weatherDayID = uri.getLastPathSegment();
                //String weatherDayID = uri.getPathSegments().get(1);
                String[] mSelectionArgs = new String[]{weatherDayID};
                cursor = db.query(WeatherContract.WeatherEntry.TABLE_NAME,
                        projection,
                        mSelection,
                        mSelectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {

        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        int match = mUriMatcher.match(uri);

        Uri insertedUri;
        switch (match) {
            case CODE_WEATHER_WITH_DATE:
                long insertedID = db.insert(WeatherContract.WeatherEntry.TABLE_NAME,
                        null,
                        values);
                if (insertedID > 0) {
                    insertedUri = Uri.withAppendedPath(WeatherContract.WeatherEntry.WEATHER_CONTENT_URI,
                            String.valueOf(insertedID));
                } else {
                    throw new SQLException("Insert failed. Please, try again.");
                }
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return insertedUri;
    }

    @Override
    public int bulkInsert(@NonNull Uri uri, @NonNull ContentValues[] values) {

        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();

        int match = mUriMatcher.match(uri);

        switch (match) {
            case CODE_WEATHER:
                db.beginTransaction();
                int rowsInserted = 0;
                try {
                    for (ContentValues val : values) {
                        long lDate = val.getAsLong(WeatherContract.WeatherEntry.COLUMN_DATE);
                        if (!SunshineDateUtils.isDateNormalized(lDate)){
                            throw new IllegalArgumentException("Bad date format.");
                        }
                        long insertedID = db.insert(WeatherContract.WeatherEntry.TABLE_NAME,
                                                                    null,
                                                                                val);
                        if (insertedID == -1){
                            throw new SQLException("Insert failed.");
                        }else {
                            rowsInserted++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                if (rowsInserted > 0){
                    getContext().getContentResolver().notifyChange(uri, null);
                }
                return rowsInserted;

            default:
                return super.bulkInsert(uri, values);

        }
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();

        int matcher = mUriMatcher.match(uri);

        int deletingCount;
        switch(matcher){
            case CODE_WEATHER_WITH_DATE:
                String mSelection = "_id=?";
                String deletingID = uri.getPathSegments().get(1);
                String[] mSelectionsArgs = new String[]{deletingID};
                deletingCount = db.delete(WeatherContract.WeatherEntry.TABLE_NAME,
                        mSelection,
                        mSelectionsArgs);
                if (deletingCount <= 0){
                    throw new SQLException("Deleting failed. Please, try again.");
                }
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri.toString());
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return deletingCount;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}
