package ru.kai.sunshine.data;

import android.net.Uri;
import android.provider.BaseColumns;

import ru.kai.sunshine.utilities.SunshineDateUtils;

/**
 * Created by dady on 13.01.2018.
 */

public class WeatherContract {

    static final String AUTHORITY = "ru.kai.sunshine";
    private static final String BASE_WEATHER_CONTENT = "content://" + AUTHORITY;
    private static final Uri BASE_WEATHER_CONTENT_URI = Uri.parse(BASE_WEATHER_CONTENT);
    static final String PATH = "WEATHER";

    public static final class WeatherEntry implements BaseColumns{
        static final String TABLE_NAME = "weather";
        public static final String COLUMN_DATE = "date";
        public static final String COLUMN_WEATHER_ID = "weather_id";
        public static final String COLUMN_MIN_TEMP = "min";
        public static final String COLUMN_MAX_TEMP = "max";
        public static final String COLUMN_HUMIDITY = "humidity";
        public static final String COLUMN_PRESSURE = "pressure";
        public static final String COLUMN_WIND_SPEED = "wind";
        public static final String COLUMN_DEGREES = "degrees";

        public static final Uri WEATHER_CONTENT_URI = BASE_WEATHER_CONTENT_URI.buildUpon()
                                                                            .appendPath(PATH)
                                                                            .build();


        public static Uri buildWeatherUriWithDate(long date) {
            return WEATHER_CONTENT_URI.buildUpon()
                    .appendPath(Long.toString(date))
                    .build();
        }

        public static String getSqlSelectForTodayOnwards() {
            long normalizedUtcNow = SunshineDateUtils.normalizeDate(System.currentTimeMillis());
            return WeatherContract.WeatherEntry.COLUMN_DATE + " >= " + normalizedUtcNow;
        }

    }


}
